window.addEventListener("beforeunload", function () {
	window.opener.postMessage("ventana_cerrada", "*");
});

function saveConfig() {
	var nombre = document.getElementById("nombre").value;
	var apellido = document.getElementById("apellido").value;
    var direccion = document.getElementById("direccion")

	localStorage.setItem("nombre", nombre);
	localStorage.setItem("apellido", apellido);
    localStorage.setItem("direccion", direccion);
	//Si activamos esta cookie solo entraremos la primera vez.
	localStorage.setItem("visited", true);
		
	window.close();
}

document.getElementById("config-form").addEventListener("submit", function (event) {
	event.preventDefault();
	saveConfig();
});