export default class Coche {
    constructor(canvas, velocity) {
      this.canvas = canvas;
      this.velocity = velocity;
  
      this.x = this.canvas.width - 1175;
      this.y = this.canvas.height - 323;
      this.width = 100;
      this.height = 90;
      this.image = new Image();
      this.image.src = "COCHE/coche.png";
  
      document.addEventListener("mousemove", this.mousemove);
    }
  
    limpiarCanvas(ctx) {
      ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    }
  
    draw(ctx) {
      this.limpiarCanvas(ctx);
      this.move();
      this.collideWithWalls();
      ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
  
    collideWithWalls() {
      const minY = 100;
      const maxY = this.canvas.height - 200;
      const minX = 0;
      const maxX = this.canvas.width - this.width;
  
      if (this.y < minY) {
        this.y = minY;
      }
  
      if (this.y > maxY) {
        this.y = maxY;
      }
  
      if (this.x < minX) {
        this.x = minX;
      }
  
      if (this.x > maxX) {
        this.x = maxX;
      }
    }
  
    move() {

    }
  
    mousemove = (event) => {
      const rect = this.canvas.getBoundingClientRect();
      const mouseX = event.clientX - rect.left;
      const mouseY = event.clientY - rect.top;
  
      // Actualizar la posición del coche con la posición del ratón
      this.x = mouseX - this.width / 2;
      this.y = mouseY - this.height / 2;
    };
  }