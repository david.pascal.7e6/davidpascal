export default class Camiones {
    constructor(canvas, velocity) {
      this.canvas = canvas;
      this.velocity = velocity;
  
      this.width = 100;
      this.height = 90;
  
      this.image = new Image();
      this.image.src = "COCHE/camion.png";
  
      this.generarPosicionInicial();
    }
  
    generarPosicionInicial() {
      const minX = this.canvas.width; // Posición inicial fuera del mapa, a la derecha
      const minY = 100; // Límite superior de la carretera
      const maxY = this.canvas.height - this.height - 200; // Límite inferior de la carretera
      this.x = minX;
      this.y = minY + Math.random() * (maxY - minY);
    }
  
    draw(ctx) {
      this.x -= this.velocity;
  
      // Verificar si el camión se salió del mapa
      if (this.x + this.width < 0) {
        this.generarPosicionInicial();
      }
  
      ctx.drawImage(this.image, this.x, this.y, this.width, this.height);
    }
  
    collideWith(sprite) {
      if (
        this.x + this.width > sprite.x &&
        this.x < sprite.x + sprite.width &&
        this.y + this.height > sprite.y &&
        this.y < sprite.y + sprite.height
      ) {
        this.mostrarGameOver();
        return true;
      } else {
        return false;
      }
    }
  
    collideWithWalls() {
      const minY = 100; // Límite superior de la carretera
      const maxY = this.canvas.height - this.height - 200; // Límite inferior de la carretera
  
      if (this.y < minY) {
        this.y = minY;
      }
  
      if (this.y > maxY) {
        this.y = maxY;
      }
    }
  
    mostrarGameOver() {
      const ctx = this.canvas.getContext("2d");
      ctx.font = "bold 48px Arial";
      ctx.fillStyle = "red";
      ctx.fillText("", this.canvas.width / 2 - 120, this.canvas.height / 2);
    }
  }