import Coche from "./coche.js";
import Camiones from "./camion.js";

const canvas = document.getElementById("game");
const ctx = canvas.getContext("2d");
canvas.width = 1200;
canvas.height = 600;

const backgrounds = new Image();
backgrounds.src = "COCHE/Carreterras.png";

let back = document.getElementById("road");

const coche = new Coche(canvas, 5);
const camiones = [];

let seconds = 0;
let timerInterval;

function generarCamiones() {
  const cantidadCamiones = Math.floor(Math.random() * 2) + 2; // Entre 2 y 3 camiones
  const espacioEntreCamiones = 200; // Espacio para que el coche pueda pasar

  let lastX = canvas.width + espacioEntreCamiones; // Posición inicial del primer camión

  for (let i = 0; i < cantidadCamiones; i++) {
    const velocidad = Math.floor(Math.random() * (5 - 3 + 1)) + 3; // Velocidad aleatoria entre 3 y 5
    const camion = new Camiones(canvas, velocidad);
    camion.x = lastX; // Posición inicial del camión
    camion.y = Math.random() * (canvas.height - camion.height);
    camiones.push(camion);

    lastX += camion.width + espacioEntreCamiones; // Actualizar la posición del siguiente camión
  }
}

function startTimer() {
  timerInterval = setInterval(() => {
    seconds++;
  }, 1000);
}

function stopTimer() {
  clearInterval(timerInterval);
}

function drawTimer() {
  ctx.fillStyle = "white";
  ctx.fillRect(canvas.width - 160, 10, 150, 40);
  ctx.font = "20px Arial";
  ctx.fillStyle = "black";
  ctx.fillText("Tiempo: " + seconds + "s", canvas.width - 150, 35);
}

function game() {
  ctx.drawImage(back, 0, 0, canvas.width, canvas.height);

  coche.draw(ctx);

  for (let i = 0; i < camiones.length; i++) {
    const camion = camiones[i];
    camion.draw(ctx);

    // Verificar colisión entre el coche y el camión
    if (camion.collideWith(coche)) {
      stopGame();
      // Realizar acciones adicionales en caso de colisión, como mostrar "Game Over"
      ctx.font = "40px Arial";
      ctx.fillStyle = "red";
      ctx.fillText("Game Over", canvas.width / 2 - 100, canvas.height / 2);
      stopTimer(); // Detener el temporizador
      return;
    }
  }

  drawTimer();

  requestAnimationFrame(game);
}

function startGame() {
  camiones.length = 0; // Limpiar los camiones existentes
  generarCamiones();
  startTimer();
  requestAnimationFrame(game);
}

function stopGame() {
  camiones.length = 0;
}

startGame();