const wrapper = document.querySelector('.wrapper');
const loginlink=document.querySelector('.login-link')
const registerLink=document.querySelector('.register-link')
const btnPoup=document.querySelector('.btnLogin-popup')
const iconClose=document.querySelector('.icon-close')

registerLink.addEventListener('click', ()=>{
    wrapper.classList.add('active');
});

loginlink.addEventListener('click', ()=>{
    wrapper.classList.remove('active');
});

btnPoup.addEventListener('click', ()=>{
    wrapper.classList.add('active-popup');
});

iconClose.addEventListener('click', ()=>{
    wrapper.classList.remove('active-popup');
});