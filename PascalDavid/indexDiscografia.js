//Classe Disco
class Disco {
    nombre;
    #Interprete;
    año;
    tipoMusica;
    portada;
    #Localización;
    #Prestado;
    get getLocalizacion() { return this.#Localización }
    get getInterprete() { return this.#Interprete }
    get getPrestado() { return this.#Prestado }
    get getinforma() {
       
    }

    constructor(nombre,Interprete, año, tipoMusica,  portada, Localizacion, informacion) {
        this.nombre = nombre;
        this.año = año;
        this.tipoMusica = tipoMusica;
        this.portada = portada;
        this.#Interprete = Interprete;
        this.#Localización = Localizacion;
        this.#Prestado = informacion;
    }
}
//Aqui esta la clase Interprete
class Interprete {
    nombre;
    apellido;
    nombreArtistico;
    constructor(nombre, apellido, nombreArtistico) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.nombreArtistico = nombreArtistico;
    }

}




// Classe discografia con sus methodos
class Discografia {
    discos;
    
    constructor(){
        this.discos=[];
    }
    
    anadir(Disco) {
        this.discos.push(Disco);
    }

    borrar(nombre){
        for (let index = 0; index < this.discos.length; index++) {
           if (nombre == this.discos[index].nombre){
            this.discos.splice(index, 1);
            
           }
        }
    }

    ordenar(atributo){
        this.discos.sort (function(a,b){
            if (a[atributo]<b[atributo])return-1;
            if (a[atributo]>=b[atributo])return 1;
        }) ;

    }

    informacion(){
        this.discos.splice(4, 1)
        return this.discos

    }

}


//Metdod que purebalos demas metodos.
function pruebas (){

    let Interprete1 = new Interprete("Adele","Laurie Blue Adkins","Adele")
    let Disco1 = new Disco("Rolling in the Deep",Interprete1, 2011, "POP","https://upload.wikimedia.org/wikipedia/en/5/5d/Adele_-_21.jpg", 1, "False")


    let Interprete2 = new Interprete("Taylor","Alison Swift","Taylor Swift")
    let Disco2 = new Disco("Shake It Off",Interprete2, 2014, "POP","https://upload.wikimedia.org/wikipedia/en/f/f6/Taylor_Swift_-_1989.png", 2, "False")


    let Interprete3 = new Interprete("Ed","Christopher Sheeran","Ed Sheeran")
    let Disco3 = new Disco("Shape of You",Interprete3,2017, "FOLK","https://upload.wikimedia.org/wikipedia/en/f/f7/Divide_cover.png", 3, "False")
  

    let Interprete4 = new Interprete("Billie","Eilish Pirate Baird O'Connell","Billie Eilish")
    let Disco4 = new Disco("Bad guy",Interprete4, 2019, "Electro","https://upload.wikimedia.org/wikipedia/en/b/be/Billie_Eilish_-_When_We_All_Fall_Asleep%2C_Where_Do_We_Go%3F.png", 4, "False")
   

    let Interprete5 = new Interprete("Aubrey","Drake Graham","Drake")
    let Disco5 = new Disco("God's Plan",Interprete5, 2018,"hip-hop","https://upload.wikimedia.org/wikipedia/en/5/5f/Drake_-_Scorpion.png", 5, "False")
  

    let D1= new Discografia ();
    D1.anadir(Disco1);
    console.log(D1)

  
    D1.anadir(Disco2);
    console.log(D1)

  
    D1.anadir(Disco3);
    console.log(D1)

    
    D1.anadir(Disco4);
    console.log(D1)

    
    D1.anadir(Disco5);
    console.log(D1)

    D1.borrar("Rolling in the Deep");
    console.log(D1)

    D1.ordenar("nombre");
    console.log(D1)

    console.log(D1.informacion());

}

pruebas();