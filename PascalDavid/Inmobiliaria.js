class Inmobiliaria{
    Inmuebles;

    constructor(Inmuebles){
        this.Inmuebles=[];
    }

    anadir(Inmueble) {
        this.Inmuebles.push(Inmueble);
    }
    listar(){
        for (let Inmueble of this.Inmuebles) {
            console.log (Inmueble);
        }
    }
    

   
}


class Inmueble {
    Direccion;
    Foto;
    Estado;
    Coordenadas;
    PrecioBase;
    RefCatastral;
    Ano;
    MCuadr;
    
    constructor(Direccion,Foto,Estado,Coordenadas,PrecioBase,RefCatastral,Ano,MCuadr){
        this.Direccion=Direccion;
        this.Foto=Foto;
        this.Estado=Estado;
        this.Coordenadas = Coordenadas;
        this.PrecioBase=PrecioBase;
        this.RefCatastral=RefCatastral;
        this.Ano=Ano;
        this.MCuadr=MCuadr;
    }

}




class Coordenadas{
    Longitud;
    Latitud;

    constructor(Longitud,Latitud){
        this.Longitud=Longitud;
        this.Latitud=Latitud;
    }
}

function Pruebas (){
    
    let Coordenadas1 = new Coordenadas("1","3")
    let Inmueble1 = new Inmueble("1","2","3", "4","5", "6", "7", "8")

    let D1= new Inmobiliaria();
    D1.anadir(Inmueble1);
    D1.listar(Inmueble1);
    console.log(D1)

}

Pruebas(); 

class Piso{
    NumPiso;
    Banos;
    Habitaciones;
    Terraza;
    Ascensor;
    
    constructor(NumPiso, Banos, Habitaciones, Terraza,Ascensor){
        this.NumPiso=NumPiso;
        this.Banos=Banos;
        this.Habitaciones=Habitaciones;
        this.Terraza=Terraza;
        this.Ascensor=Ascensor;
    }
}

class Casa {
    Banos;
    Habitaciones;
    Jardin;
    TipoCasa;
    constructor(Banos, Habitaciones, Jardin, TipoCasa) {
        this.Banos = Banos;
        this.Habitaciones = Habitaciones;
        this.Jardin = Jardin;
        this.TipoCasa = TipoCasa;
    }
}

class Jardin{
    Existe;
    Area;
    Piscina;
    constructor(Existe, Area, Piscina){
        this.Area = Area;
        this.Existe = Existe;
        this.Piscina = Piscina;
    }
}


class Local{
    NumVentanas;
    TipoLocal;
    VentanasMetalicas;
    
    constructor(NumVentanas,TipoLocal,VentanasMetalicas){
        this.NumVentanas=NumVentanas;
        this.TipoLocal=TipoLocal;
        this.VentanasMetalicas=VentanasMetalicas;
    }
}

class TipoLocal{
    Comerciales;
    Imdustriales;
    Restauracion;

    constructor(Comerciales,Industriales,Restauracion){
        this.Comerciales=Comerciales;
        this.Industriales=Industriales;
        this.Restauracion=Restauracion;
    }
}

class Industriales{
    Urbano;

    constructor (Urbano) {
        this.Urbano=Urbano;
    }
}


