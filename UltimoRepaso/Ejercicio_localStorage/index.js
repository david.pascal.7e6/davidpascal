// Función para mostrar el modal de configuración si es la primera vez que se visita la página
function showConfigModal() {
    //localStorage.removeItem("visited");
    var visited = localStorage.getItem("visited");
    console.log(visited);
    console.log(localStorage);
    if (!visited) {
        //abrimos ventana de configuración
        window.open('./popUP.html', 'Config', 'toolbar=yes,statusbar=yes,width=600,height=400')

        //Especificamos el evento que nos indica que la ventana ha sido cerrada
        window.addEventListener("message", function (event) {
            if (event.data === "ventana_cerrada") {
                changeConfig();
            }
        });

    }
    else{changeConfig()}
}
showConfigModal();

function changeConfig() {
    console.log("Entra Config");
    // Aplicamos la configuración guardada en la cookie a la página principal
    var backgroundColor = localStorage.getItem("background-color");
    var fontColor = localStorage.getItem("font-color");
    //console.log(backgroundColor);
    if (backgroundColor) {
        document.body.style.backgroundColor = backgroundColor;
    }
    if (fontColor) {
        document.body.style.color = fontColor;
    }
}