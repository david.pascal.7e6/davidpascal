<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
       <html>
           <head>
               <title>Mundo</title>
           </head>
           <body>
               <h1>Continentes</h1>


               <xsl:apply-templates select="mundo/continente"/>
           </body>
       </html>
</xsl:template>

   <xsl:template match="mundo/continente">
   <h2>
       <xsl:value-of select="nombre" />
   </h2>
   <table border="1">
       <xsl:attribute name="bordercolor">
           <xsl:value-of select="nombre/@color" />
       </xsl:attribute>
       <tr>
           <th>Bandera</th>
           <th>País</th>
           <th>Gobierno</th>
           <th>Capital</th>
       </tr>
      
       <xsl:for-each select="paises/pais">
        <xsl:sort select="nombre"/>
          <tr>
               <td>
                   <img>
                       <xsl:attribute name="src"> 
                       img/<xsl:value-of select="foto" />
                       </xsl:attribute>
                   </img>
               </td>
                <td>
                    <xsl:value-of select="nombre"/>
                </td>
                  <td>
                    <xsl:choose>
                        <xsl:when test="nombre[@gobierno='republica']">
                        <xsl:attribute name="style">background-color: yellow</xsl:attribute>
                        </xsl:when>
                        <xsl:when test="nombre[@gobierno='dictadura']">
                        <xsl:attribute name="style">background-color: red</xsl:attribute>
                        </xsl:when>
                        <xsl:otherwise>
                         <xsl:attribute name="style">background-color: white</xsl:attribute>
                        </xsl:otherwise>
                    </xsl:choose>
                <xsl:value-of select="nombre/@gobierno"/>
                </td>
                  <td>
                    <xsl:value-of select="capital"/>
                </td>
        </tr>
       </xsl:for-each>
   </table>
     </xsl:template>
</xsl:stylesheet>