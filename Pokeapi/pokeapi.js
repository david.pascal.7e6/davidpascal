const listaPokemon = document.querySelector("#listaPokemon");
const botonesHeader = document.querySelectorAll(".btn-header");
let URL = "https://pokeapi.co/api/v2/pokemon/";
const searchForm = document.querySelector("#search-form");
const searchInput = document.querySelector("#search-input");

// Agregar un evento de escucha al formulario para buscar un Pokémon por nombre
searchForm.addEventListener("submit", (event) => {
    event.preventDefault(); // Evitar que el formulario se envíe automáticamente

    const pokemonName = searchInput.value.toLowerCase(); // Obtener el nombre del Pokémon ingresado
    const pokemonUrl = `https://pokeapi.co/api/v2/pokemon/${pokemonName}`; // Construir la URL de la API

    fetch(pokemonUrl)
        .then((response) => {
            if (!response.ok) {
                throw new Error("No se encontró el Pokémon");
            }
            return response.json();
        })
        .then((data) => {
            // Generar la ficha del Pokémon encontrado y mostrarla en el contenedor correspondiente
            const pokemonCard = generatePokemonCard(data);
            listaPokemon.innerHTML = "";
            listaPokemon.appendChild(pokemonCard);
        })
        .catch((error) => {
            console.log(error);
            alert("No se encontró el Pokémon");
        });

    // Limpiar el campo de entrada después de buscar
    searchInput.value = "";
});

// Función para generar la ficha de un Pokémon
function generatePokemonCard(poke) {
    // Verificar si la propiedad 'types' existe y tiene datos válidos
    const tipos = poke.types ? poke.types.map((type) => `<p class="${type.type.name} tipo">${type.type.name}</p>`).join('') : '';
  
    let pokeId = poke.id.toString();
    if (pokeId.length === 1) {
      pokeId = "00" + pokeId;
    } else if (pokeId.length === 2) {
      pokeId = "0" + pokeId;
    }
  
    const div = document.createElement("div");
    div.classList.add("pokemon");
    div.innerHTML = `
      <p class="pokemon-id-back">${pokeId}</p>
      <div class="pokemon-imagen">
          <img src="${poke.sprites.other["official-artwork"].front_default}" alt="${poke.name}">
      </div>
      <div class="pokemon-info">
          <div class="nombre-contenedor">
              <p class="pokemon-id">#${pokeId}</p>
              <h2 class="pokemon-nombre">${poke.name}</h2>
          </div>
          <div class="pokemon-tipos">
              ${tipos}
          </div>
      </div>
    `;
  
    return div;
  }


for (let i = 1; i <= 151; i++) {
    fetch(URL + i)
        .then((response) => response.json())
        .then(data => mostrarPokemon(data))
}

function mostrarPokemon(poke) {
    let tipos=poke.types.map((type) => `<p class="${type.type.name} tipo">${type.type.name}</p>`);
    tipos=tipos.join('');

    let pokeId = poke.id.toString();
    if(pokeId.length ===1){
        pokeId="00"+ pokeId;
    }else if(pokeId.length ===2){
        pokeId="0"+pokeId;
    }

    const div = document.createElement("div");
    div.classList.add("pokemon");
    div.innerHTML = `
    <p class="pokemon-id-back">${pokeId}</p>
    <div class="pokemon-imagen">
        <img src="${poke.sprites.other["official-artwork"].front_default}" alt="${poke.name}">
    </div>
    <div class="pokemon-info">
        <div class="nombre-contenedor">
            <p class="pokemon-id">#${pokeId}</p>
            <h2 class="pokemon-nombre">${poke.name}</h2>
        </div>
        <div class="pokemon-tipos">
            ${tipos}
        </div>
        <div class="pokemon-stats">
            <p class="stat">${poke.height}</p>
            <p class="stat">${poke.weight}</p>
        </div>
    </div>
    `;
    listaPokemon.append(div);
}
botonesHeader.forEach(boton => boton.addEventListener("click", (event) => {
    const botonId= event.currentTarget.id;

    listaPokemon.innerHTML="";
    for (let i = 1; i <= 151; i++) {
        fetch(URL + i)
            .then((response) => response.json())
            .then(data => {
                
                if(botonId === "ver-todos"){
                    mostrarPokemon(data)
                }

                const tipos=data.types.map(type=>type.type.name);
                if(tipos.some(tipo=>tipo.includes(botonId))){
                    mostrarPokemon(data)
                }
    
            })
    }
}));