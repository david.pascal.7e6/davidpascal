<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <?xml-stylesheet href="tasks.css"?>
     <xsl:template match="/">
       <html>
           <head>
               <title>Mundo</title>
                <link rel="stylesheet" href="tasks.css"></link>
           </head>
           <body>
          
     <h2><xsl:value-of select="taskList/@owner"/></h2>
    <h3><xsl:choose><xsl:when test="taskList/@mes='3'">MES de Marzo</xsl:when></xsl:choose></h3>


        <xsl:apply-templates select="taskList/task"/>
                
                 <table border="1"></table>


           </body>
       </html>


   </xsl:template>
   <xsl:template match="taskList/task">
   <br>   </br>
      <table border="1">
          <tr>
           <th><xsl:value-of select="date"/></th>
           <th class="hashtag"><xsl:value-of select="coments/Hashtag"/></th>
          </tr>
           <tr>
               <td class="title">
                  <xsl:value-of select="title" />
               </td>
               <td>
               <xsl:for-each select="items/item">
                <ul>
                     <li>
                    <a><xsl:value-of select="text()"/></a>
                    </li>
               </ul>
               </xsl:for-each>
               </td>
               <br></br>
               <tr>
              <td class="nota">             
                <xsl:value-of select="coments/nota" />       
             </td>
             </tr>
           </tr>

   </table>
   </xsl:template>


</xsl:stylesheet>
