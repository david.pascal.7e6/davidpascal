<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
       <html>
           <head>
               <title>Mundo</title>
           </head>
           <body>
               <h1>Continentes</h1>


               <xsl:apply-templates select="mundo/continente"/>




           </body>
       </html>


   </xsl:template>




   <xsl:template match="mundo/continente">
   <h2>
       <xsl:value-of select="nombre" />
   </h2>
   <table border="1">
       <xsl:attribute name="bordercolor">
           <xsl:value-of select="nombre/@color" />
       </xsl:attribute>
       <tr>
           <th>Bandera</th>
           <th>País</th>
           <th>Gobierno</th>
           <th>Capital</th>
       </tr>
      
       <xsl:for-each select="paises/pais">
       <xsl:sort select="nombre"></xsl:sort>
           <tr>
               <td>
                   <img>
                       <xsl:attribute name="src">
                           img/<xsl:value-of select="foto" />
                       </xsl:attribute>
                   </img>
               </td>
               <td>
                   <xsl:value-of select="nombre" />
               </td>


               <td>
               <xsl:choose>
                   <xsl:when test="nombre/@gobierno='dictadura'">
                       <xsl:attribute name="style">background-color: red</xsl:attribute>
                   </xsl:when>
                   <xsl:when test="nombre/@gobierno='monarquia'">
                       <xsl:attribute name="style">background-color: yellow</xsl:attribute>
                   </xsl:when>
               </xsl:choose>
               <xsl:value-of select="nombre/@gobierno" />
           </td>
           <td>
               <xsl:value-of select="capital" />
           </td>
           </tr>
       </xsl:for-each>
   </table>
   </xsl:template>


</xsl:stylesheet>


<?xml version="1.0" encoding="UTF-8"?>
<!-- Declaración XML con la versión y la codificación -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<!-- Declaración de la hoja de estilo XSLT con la versión 1.0 y el espacio de nombres "xsl" -->

   <xsl:template match="/">
   <!-- Plantilla que coincide con la raíz del documento XML -->

       <html>
           <head>
               <title>Mundo</title>
           </head>
           <body>
               <h1>Continentes</h1>
               <!-- Estructura HTML básica con un encabezado y título -->

               <xsl:apply-templates select="mundo/continente"/>
               <!-- Aplica las plantillas a los elementos "continente" dentro de "mundo" -->

           </body>
       </html>

   </xsl:template>

   <xsl:template match="mundo/continente">
   <!-- Plantilla que coincide con los elementos "continente" dentro de "mundo" -->

   <h2>
       <xsl:value-of select="nombre" />
   </h2>
   <!-- Encabezado de nivel 2 con el valor del elemento "nombre" -->

   <table border="1">
       <xsl:attribute name="bordercolor">
           <xsl:value-of select="nombre/@color" />
       </xsl:attribute>
       <!-- Tabla con un atributo "bordercolor" obtenido del atributo "color" del elemento "nombre" -->

       <tr>
           <th>Bandera</th>
           <th>País</th>
           <th>Gobierno</th>
           <th>Capital</th>
       </tr>
       <!-- Fila de encabezado de la tabla -->

       <xsl:for-each select="paises/pais">
       <!-- Iteración sobre los elementos "pais" dentro de "paises" -->

       <xsl:sort select="nombre"></xsl:sort>
       <!-- Ordena los elementos "pais" por el valor del elemento "nombre" -->

           <tr>
               <td>
                   <img>
                       <xsl:attribute name="src">
                           img/<xsl:value-of select="foto" />
                       </xsl:attribute>
                   </img>
               </td>
               <!-- Celda de imagen con la ruta de la imagen obtenida del elemento "foto" -->

               <td>
                   <xsl:value-of select="nombre" />
               </td>
               <!-- Celda con el valor del elemento "nombre" -->

               <td>
               <xsl:choose>
                   <xsl:when test="nombre/@gobierno='dictadura'">
                       <xsl:attribute name="style">background-color: red</xsl:attribute>
                   </xsl:when>
                   <xsl:when test="nombre/@gobierno='monarquia'">
                       <xsl:attribute name="style">background-color: yellow</xsl:attribute>
                   </xsl:when>
               </xsl:choose>
               <xsl:value-of select="nombre/@gobierno" />
               </td>
               <!-- Celda con el valor del atributo "gobierno" del elemento "nombre" y un estilo condicional -->
               <td>
                   <xsl:value-of select="capital" />
               </td>
               <!-- Celda con el valor del elemento "capital" -->

           </tr>
           <!-- Fin de la iteración sobre los elementos "pais" -->
           
       </xsl:for-each>
       <!-- Fin del bucle for-each -->

   </table>
   <!-- Fin de la tabla -->

   </xsl:template>
   <!-- Fin de la plantilla para los elementos "continente" -->

</xsl:stylesheet>
<!-- Fin de la hoja de estilo XSLT -->