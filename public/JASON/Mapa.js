var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    


     
       
        fetch('bicing.json')
         .then(function (response) {
            return response.json();
        })
        
        .then(function (dataJson) {
            for (const item of dataJson.stations) {
                  if (item.bikes>"10")
                  {
                  let circle = L.circle([item.latitude, item.longitude], {
                                color: 'green',
                                fillColor: 'green',
                                fillOpacity: 0.5,
                                radius: 500
                            }).addTo(map);
                   } 
                        
                    }
                })