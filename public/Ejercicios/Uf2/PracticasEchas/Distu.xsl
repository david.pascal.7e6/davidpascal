<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="xml" indent="yes"/>
<xsl:template match="/">
    <xsl:element name="lista">
    <xsl:for-each select="discos/disco">
    <disco>
    <xsl:value-of select="title"/>es interpretado por:
            <xsl:variable name="id" select="interpreter/@id" />
            <xsl:variable name="tupap" select="/discos/group[@id=$id]/name" />
            <xsl:value-of select="$tupap"/>
    </disco>
    </xsl:for-each>
    </xsl:element>
  </xsl:template>
</xsl:stylesheet>
   