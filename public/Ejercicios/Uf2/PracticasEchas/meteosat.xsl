<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  


<xsl:template match="/">

    <html>

    <body>
      <table border="1">
        <tr>
          <th>FECHA</th>
          <th>MAXIMA</th>
          <th>MINIMA</th>
          <th>PREDICCION</th>
        </tr>
        
        
        <xsl:for-each select="root/prediccion/dia">
        <xsl:sort>
          <tr>
            <td>
              <xsl:value-of select="@fecha"/>
            </td>
            <td>
              <xsl:value-of select="temperatura/maxima"/>
            </td>

           <td>
              <xsl:value-of select="temperatura/minima"/>
            </td>
            <td>
              <xsl:value-of select="estado_cielo/@descripcion"/>
            </td>
            <td>
              <img src="{concat('imagenes/',estado_cielo[@periodo='00-24']/@descripcion)}.png"/>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </body>

    </html>
  </xsl:template>
</xsl:stylesheet>