<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
  


<xsl:template match="/">

    <html>

    <body>
      <table border="1">
        <tr style="background-color: lightgreen">
          <th>NOMBRE</th>
          <th>APELLIDOS</th>
          <th>TELEFONO</th>
          <th>REPETIDOR</th>
          <th>NOTA PRACTICA</th>
          <th>NOTA EXAMEN</th>
          <th>NOTA TOTAL</th>
          <th>FOTO</th>
        </tr>
        
        
        <xsl:for-each select="evaluacion/alumno">
        <xsl:sort select="apellidos" />

          <tr>
            <td>
              <xsl:value-of select="nombre"/>
            </td>
            <td>
              <xsl:value-of select="apellidos"/>

            </td>

           <td>
              <xsl:value-of select="telefono"/>
            </td>
                 <td>
              <xsl:value-of select="@repite"/>
            </td>
                 <td>
              <xsl:value-of select="notas/practicas"/>
            </td>
            <td>
              <xsl:value-of select="notas/examen"/>
            </td>
            <xsl:variable name="final" select="(./notas/examen + ./notas/practicas) div 2"/>
             <xsl:choose>
              
                   <xsl:when test="$final &lt; 5"><td style="background-color: red"> <xsl:value-of select="$final"/></td> </xsl:when>
  
                    <xsl:when test="$final &gt; 7.9"><td style="background-color: lightblue"><xsl:value-of select="$final"/></td></xsl:when>
      
           <xsl:otherwise><td><xsl:value-of select="$final"/></td></xsl:otherwise>
       </xsl:choose>
            <td>
            <img width="70px" height="70px">
            <xsl:attribute name="src">
            <xsl:value-of select="imagen"/>
            </xsl:attribute>
              </img>
            </td>
          </tr> 
        </xsl:for-each>
      </table>
    </body>
    </html>
  </xsl:template>
</xsl:stylesheet>