<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">
<html>
    <body>
    <main>

    <xsl:for-each select="mundo/continente">
    <h1><xsl:value-of select="nombre"/></h1>
     
        <table border="1" style="border-color:nombre/@color">
>
               <tr>
                <th>BANDERA</th>
                <th>País</th>
                <th>Gobierno</th>
               <th>Capital</th>
              </tr>

    <xsl:for-each select="paises/pais">
     <xsl:sort select="nombre" />
       


          <tr>
            <td>      
           <img>
            <xsl:attribute name="src">
            <xsl:value-of select="foto"/>
            </xsl:attribute>
              </img>
            </td>
            <td> <xsl:value-of select="nombre"/>  </td>
             <td> <xsl:value-of select="nombre/@gobierno"/>  </td>
           
           <xsl:variable name="final" select="nombre/@gobierno"/>
             <xsl:choose>
              
                   <xsl:when test="$final = monarquía"><td style="background-color: yellow"> <xsl:value-of select="$final"/></td> </xsl:when>
  
                    <xsl:when test="$final = dictadura "><td style="background-color: red"><xsl:value-of select="$final"/></td></xsl:when> 
              
              </xsl:choose>  
               
                 
                 <td> <xsl:value-of select="capital"/> </td>
           
          
          </tr> 
           
            </xsl:for-each>
            </table> 
        </xsl:for-each>
             
 </main>
    </body>
</html>
  </xsl:template>
</xsl:stylesheet>