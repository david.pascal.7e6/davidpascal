<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:output method="html"/>
<xsl:template match="/">

<html>
<head>
    <meta charset="UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <link rel="stylesheet" href="PaginaPrinc.css"/>
    <title>Ejercicios de clase </title>
</head>
<body>
<header class="heat">
        <img class="imgTit" src="M4PLay.png" alt=""/>
        <h1>LEARNING THROUGH PLAY</h1>
    </header>
    <nav>
        <ul class="Navegador">
            <li><a href="PaginaPrinc.html">M04PLAY</a></li>
            <li><a href="PaginaTerciaria.html">VIDEOJUEGOS</a></li>
            <li><a href="PaginaSecundaria.html">NOTICIAS</a></li>
            <li><a href="PaginaPrinc.html">RANKING</a></li>
            <li><a href="Cuestionario.html">ENQUESTA</a></li>
            <li><a href="Cuestionario.html">CONTACTO</a></li>
        </ul>
    </nav>
  <main>
  <h2>Videojuegos</h2>
    <section >
    <div class="cajita1">
    <xsl:for-each select="videojuegos/videojuego">
      <article class="primerA" >
      <img>
            <xsl:attribute name="src">
            <xsl:value-of select="imagen"/>
            </xsl:attribute>
              </img>
      <h3 ><xsl:value-of select="Nombre"/></h3>
      <div class="paragrafos">
      <p ><xsl:value-of select="InfoGeneral/Descripcion"/></p>
       </div>
       <div class="anchor">
            <a href="#" class="b"><xsl:value-of select="Mas"/></a>
         </div>
          
      </article >
      </xsl:for-each>
      </div>
    </section>
     </main>
         <footer class="Pie2">
        <ul>
            <li>
                <a><img class="Fo" src="facebook.png"/></a>
            </li>
            <li>
                <a><img class="Fo" src="twitter.png"/></a>
            </li>
            <li>
                <a><img class="Fo" src="youtube.png"/></a>
            </li>
            <li>
                <a><img class="Fo" src="instagram.png"/></a>
            </li>
        </ul>
        <address>
            <a>Carrer Aiguablava 08033 Barcelona</a>
        </address>
        <p>Copyright &#169; 2022 - 2022</p>

    </footer>
    </body>
 </html>
 
  </xsl:template>
</xsl:stylesheet>